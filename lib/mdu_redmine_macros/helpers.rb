module MDU
  module RedmineMacros
    module MDUHelpers
      def latest_ticket_of_type(project, tracker)
        issues = Issue.where(tracker: tracker, project: project).all
        issues = issues.sort_by { |i| i.id }
        # Return the last (most recent) issue
        issues.last
      end

      def uk_date_string(d)
        return '(no date)' unless d
        months = %w[(skip) Jan Feb Mar Apr May June July Aug Sept Oct Nov Dec]
        fmt = "%d #{months[d.month]} %Y"
        d.strftime(fmt)
      end
    end
  end
end
