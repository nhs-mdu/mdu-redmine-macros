plugin_name = 'mdu-redmine-macros'

Rails.configuration.to_prepare do
  require_dependency "mdu_redmine_macros/project_field_macro"
  require_dependency "mdu_redmine_macros/project_members_macro"
  require_dependency "mdu_redmine_macros/project_summary_box_macro"
  require_dependency "mdu_redmine_macros/projects_summary_macro"
  require_dependency "mdu_redmine_macros/helpers"
end

Redmine::Plugin.register plugin_name do
  requires_redmine version_or_higher: '4.2'
  name 'MDU Redmine Custom Macros'
  author 'Patrick Skillen'
  description 'Wiki macros to support MDU QMS via Redmine.'
  version '0.3.1'
  url 'https://gitlab.com/nhs-mdu/mdu-redmine-macros'
  author_url 'https://gitlab.com/nhs-mdu/mdu-redmine-macros'
end
