# MDU Redmine Macros

## 2021-05-12 - v0.3.1

* Redmine version: `v4.2`

**Changes:**

* Fix min Redmine version (4.2+)
* Fix version number in Redmine admin display

## 2021-05-12 - v0.3.0

* Redmine version: `v4.2`

**Changes:**

* Fix `users_by_role` -> `principals_by_role` (changed in Redmine 4.2)

## 2021-05-12 - earlier versions...

Changelog started
